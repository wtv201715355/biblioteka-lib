#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Osoba {
    int id;
    char ime[20];
    char prezime[20];
};
struct Datum {
    int dan;
    int mjesec;
    int godina;
};
struct Knjiga {
    int id;
    char naslov[50];
    char autori[100];
    char isbn[20];
    int posudjena;
    struct Datum datum_posudbe;
};
void unesi(char* string, int vel);
void unos_osobe (struct Osoba osobe [], int rb);
int ispravnost_id_osobe (struct Osoba osobe [], int id, int vel);
void edituj_osobu (struct Osoba osoba [], int id, const int velo, int a);
int izbrisi_osobu (struct Osoba osobe [], const struct Knjiga knjige [], int id, int vel1, int vel2);
int nadji_id_osobe (struct Osoba osobe [],const int id, const int vel);
void ispisi_osobu (struct Osoba niz [], int adresa, int vel);
void unos_knjige (struct Knjiga knjige [], int rb);
int ispravnost_id_knjige (struct Knjiga knjiga [], int id, int vel);
void edituj_knjigu  (struct Knjiga knjiga [], int id, int a);
int izbrisi_knjigu  (struct Knjiga knjiga [],  int a, int vel1);
int nadji_id_knjige (struct Knjiga knjiga [],const int id, const int vel);
void zaduzi_knjigu (struct Knjiga knjiga [], int id, int a);
void vrati_knjigu (struct Knjiga knjiga [], int id, int a);
void pretraga_po_imenu_i_prezimenu_osobe (const struct Osoba osobe [],const char prezime [], const char ime [], const int vel);
void pretraga_po_nazivu_autorima_isbnu_knjige (const struct Knjiga knjige [],const char naslov [],const char autori [], const char isbn [], const int vel);
int duzan_knjigu (const struct Knjiga knjige [],const int id, const int vel);
int ispravnost_datuma (struct Datum datum);
int prestupna (int godina);
int godine_i_mjeseci_u_dane (struct Datum datum);
int proslo_dana (struct Datum datum);
void sortiraj (struct Knjiga knjige[], const int velk);
void ispis_kniga_nadogradjen  (struct Knjiga knjige [],struct Osoba osobe [], int velk, int velo);
struct Osoba vrati_osobu_po_id (struct Osoba osobe [],const int id, const int vel);
void ispisi_razmak (int razmaka);
void upisi_razmak (char razmaci [],int razmaka);
void ispisi_osobu (struct Osoba niz [], int adresa, int vel);
void ispisi_knjigu (struct Knjiga niz[], int adresa, int vel);
void zamjeni_razmak_ (char *string);
void zamjeni__razmakom (char *string);
int main() {
    struct Osoba nizo [256];
    struct Knjiga nizk [1024];
    /*zbog problema koji mogu nastati ako budemo orginalni niz sortirali - sortirat cemo pomocni niz sort*/
    struct Knjiga nizksort [1024];
    int i=0, velo=0, ido, adresao;
    char ime [20], prezime [20], naslov[50], autori [100], isbn [20];
    int velk=0, idk, adresak;
    /*ideja je da prvo ocitamo sadrzaj datoteka*/
    FILE *knjigedat=fopen("C:\\knjige.txt", "a+");
    FILE *osobedat=fopen("C:\\osobe.txt", "a+");
    FILE *broj=fopen("C:\\ukupno.txt", "a+");
    if ((knjigedat==NULL)||(osobedat==NULL)||broj==NULL) {
        if (knjigedat!=NULL)
            fclose(knjigedat);
        if (osobedat!=NULL)
            fclose(osobedat);
        if (broj!=NULL)
            fclose(broj);
        printf("datoteka nije kreirana i to znaci da cemo zavrsit sa programom!\n");
        return EXIT_FAILURE;
    }
    while (fscanf(osobedat,"%d %20s %20s\n", &nizo[i].id, nizo[i].ime, nizo[i].prezime)==3 && i<256)
        i++;
    velo=i;
    i=0;
    fscanf(broj,"%d", &velk);
    for (i=0;i<velk;) {
        fscanf(knjigedat,"%d %50s %100s %20s %d %d.%d.%d\n", &nizk[i].id, nizk[i].naslov, nizk[i].autori, nizk[i].isbn, &nizk[i].posudjena, &nizk[i].datum_posudbe.dan, &nizk[i].datum_posudbe.mjesec, &nizk[i].datum_posudbe.godina);
        if (fgetc(knjigedat)==EOF) break;
        i++;
    }
    /*zatim da obrisemo datoteke*/
    remove("C:\\knjige.txt.txt");
    remove("C:\\osobe.txt");
    remove("C:\\broj.txt");
    fclose(knjigedat);
    fclose(osobedat);
    fclose(broj);
    /*nakon toga kreiramo nove datoteke koje se zovu kao i obrisane - a na taj nacin uvijek imamo uvijek svjeze podatke*/
    FILE *noveknjige=fopen("C:\\knjige.txt", "w");
    FILE *noveosobe=fopen("C:\\osobe.txt", "w");
    FILE *novibroj=fopen("C:\\ukupno.txt", "w");
    if ((noveknjige==NULL)||(noveosobe==NULL)) {
        if (noveknjige!=NULL)
            fclose(noveknjige);
        if (noveosobe!=NULL)
            fclose(noveosobe);
        printf("datoteka nije kreirana i to znaci da cemo zavrsit sa programom!\n");
        return EXIT_FAILURE;
    }
    /*posto smo zamijenili u datoteci sve razmake zamijenili razmake sa _ potrebno je to popravit*/
    for (i=0;i<velk;i++) {
        zamjeni__razmakom (nizk[i].naslov);
        zamjeni__razmakom (nizk[i].autori);
        zamjeni__razmakom (nizk[i].isbn);
    }
    printf("Unesi 1 za unos osobe, 2 za editovanje osobe, 3 za brisanje osobe\n\n");
    printf("Unesi 4 za unos knjige, 5 za editovanje knjige, 6 za brisanje knjige\n7 za iznajmljivanje knjige, 8 za vracanje knjige\n\n");
    printf("Unesi 9 za pretragu niza osoba po id, \n10 za pretragu niza osoba po imenu i prezimenu\n\n");
    printf("Unesi 11 za pretragu niza knjiga po id, \n12 za pretragu niza knjiga po naslovu i autorima i isbnu\n\n");
    printf("Za ispis unesi 13\n\n");
    do {
        scanf("%d", &i);
        if (i==1) {
            unos_osobe(nizo, velo);
            velo++;
        } else if (i==2) {
            printf("Unesi id osobe kojoj zelite da promjenite podatke\n");
            scanf("%d", &ido);
            adresao=nadji_id_osobe(nizo, ido, velo);
            edituj_osobu(nizo, ido, velo, adresao);
        } else if (i==3) {
            printf("Unesi id osobe koju zelite da obrisete\n");
            scanf("%d", &ido);
            adresao=nadji_id_osobe(nizo, ido, velo);
            velo=izbrisi_osobu (nizo, nizk, ido, velo, velk) ;
        }
        if (i==4) {
            unos_knjige(nizk, velk);
            velk++;
        } else if (i==5) {
            printf("Unesi id knjige kojoj zelite da promjenite podatke\n");
            scanf("%d", &idk);
            adresak=nadji_id_knjige(nizk, idk, velk);
            edituj_knjigu(nizk, idk, adresak);
        } else if (i==6) {
            printf("Unesi id knjige koju zelite da obrisete\n");
            scanf("%d", &idk);
            adresak=nadji_id_knjige(nizk, idk, velk);
            velk=izbrisi_knjigu(nizk,adresak, velk);
        } else if (i==7) {
            printf("Unesite id knjige koju zelite iznajmit\n");
            scanf("%d", &idk);
            adresak=nadji_id_knjige(nizk,idk,velk);
            printf("Unesite id osobe kojoj zelite iznajmit\n");
            scanf("%d", &ido);
            adresao=nadji_id_osobe(nizo, ido, velo);
            zaduzi_knjigu (nizk, ido, adresak);
        } else if (i==8) {
            printf("Unesite id knjige koju zelite vratit\n");
            scanf("%d", &idk);
            adresak=nadji_id_knjige(nizk, idk, velk);
            vrati_knjigu (nizk, idk, adresak);
        } else if (i==9) {
            printf("Unesite id osobe koju zelite da nadjete\n");
            scanf("%d", &ido);
            adresao=nadji_id_osobe(nizo, ido, velo);
            ispisi_osobu (nizo, adresao, velo);
        } else if (i==10) {
            printf("Unesite ime osobe koju zelite da nadjete \n- ako ne trazite po imenu pritisnite 0\n");
            scanf("%19s", ime);
            printf("Unesite prezime osobe koju zelite da nadjete \n- ako ne trazite po prezimenu pritisnite 0\n");
            scanf("%19s", prezime);
            pretraga_po_imenu_i_prezimenu_osobe (nizo,prezime, ime , velo);
        } else if (i==11) {
            printf("Unesite id knjige koju zelite da nadjete\n");
            scanf("%d", &idk);
            adresak=nadji_id_knjige(nizk,idk,velk);
            ispisi_knjigu (nizk, adresak, velk);
        } else if (i==12) {
            getchar();
            printf("Unesite naslov knjige koju zelite da nadjete\n");
            unesi(naslov, 50);
            printf("Unesite autore knjige koju zelite da nadjete\n");
            unesi(autori, 100);
            printf("Unesite isbn knjige koju zelite da nadjete\n");
            unesi(isbn, 20);
            pretraga_po_nazivu_autorima_isbnu_knjige (nizk, naslov, autori, isbn, velk);
        }  else if (i==13) {
            i=0;
            for (i=0;i<velk;i++)
                nizksort[i]=nizk[i];
            sortiraj(nizksort, velk);
            ispis_kniga_nadogradjen  (nizksort, nizo, velk, velo);
        }
    } while (i!=0);
    /*zbog toga sto prilikom upisa iz datoe koristimo funkciju fscanf*/
    for (i=0;i<velk;i++) {
        zamjeni_razmak_ (nizk[i].naslov);
        zamjeni_razmak_ (nizk[i].autori);
        zamjeni_razmak_ (nizk[i].isbn);
        fprintf(noveknjige,"%d %-50s %-100s %-20s %d ", nizk[i].id, nizk[i].naslov, nizk[i].autori, nizk[i].isbn, nizk[i].posudjena);
        fprintf(noveknjige,"%d.%d.%d.\n", nizk[i].datum_posudbe.dan, nizk[i].datum_posudbe.mjesec, nizk[i].datum_posudbe.godina);
    }
    for (i=0;i<velo;i++) {
        fprintf(noveosobe,"%d %-20s %-20s\n", nizo[i].id, nizo[i].ime, nizo[i].prezime);
    }
    fprintf(broj,"%d", velk);
    fclose(noveknjige);
    fclose(noveosobe);
    fclose(novibroj);
    i=0;
    return 0;
}
/*iskoristicemo funkciju sa tutorijala 10*/
void unesi(char* string, int vel) {
    int i=0;
    do {
        *string++ = getchar();
        i++;
        if (i==vel) break;
    } while (*(string-1) != '\n');
    *(string-1) = '\0';
}
/*funkcija za onos nove osobe u niz, rb je redni broj osobe u nizu*/
void unos_osobe (struct Osoba osobe [], int rb) {
    if (rb<256) {
        do {
            printf("Unesi id osobe\n");
            scanf("%d", &osobe[rb].id);
        } while (!(ispravnost_id_osobe(osobe, osobe[rb].id, rb)));
        getchar();
        printf("Unesi ime osobe\n");
        unesi(osobe[rb].ime, 20);
        printf("Unesi prezime osobe\n");
        unesi(osobe[rb].prezime, 20);
    } else
        printf("Napunili smo spremnik!!!\n");
}
/*funkcija za provjeru ispravnosti id osobe*/
int ispravnost_id_osobe (struct Osoba osobe [], int id, int vel) {
    int i;
    for (i=0;i<vel;i++) {
        if ((osobe[i].id==id)||(osobe[i].id<1)) {
            return 0;
        }
    }
    return 1;
}
/*funkcija koja omogucava promjenu imena i prezimena osobi*/
void edituj_osobu (struct Osoba osoba [], int id, const int velo, int a) {
    int i;
    printf("Za promjenu imena pritisnite 1\nZa promjenu prezimena pritisnite 2\n");
    if (a>-1) {
        do {
            if (i==1) {
                printf("Unesite promjenjeno ime:\n");
                scanf("%19s",osoba[a].ime);
            } else if (i==2) {
                printf("Unesite promjenjeno prezime:\n");
                scanf("%19s",osoba[a].prezime);
            }
            printf("Kad promjenite sve sto ste zeljeli kod osobe pritisnite 0\n");
            scanf("%d", &i);
        } while (i!=0);
    } else
        printf("Osoba nije pronadjena sa tim id pa nemoze biti izvrsena izmjena!\n");
}
/*funkcija koja brise osobu ukoliko ona nema kod sebe knjiga, i ukoliko postoji*/
int izbrisi_osobu (struct Osoba osobe [], const struct Knjiga knjige [], int id, int vel1, int vel2) {
    int i=0, j, zaduzen=0;
    for (i=0;i<vel1;i++) {
        if (osobe[i].id==id) {
            zaduzen=duzan_knjigu(knjige, id, vel2);
            if (!zaduzen) {
                for (j=i;j<vel1-1; j++)
                    osobe[j]=osobe[j+1];
                vel1--;
            }
        }
    }
    if (zaduzen)
        printf("Duzan/a je barem jednu knjigu i nece biti izbrisan/a!\n");
    return vel1;
}
/*pronalazi osobu na osnovu id*/
int nadji_id_osobe (struct Osoba osobe [],const int id, const int vel) {
    int i=0;
    for (;i<vel;i++) {
        if (osobe[i].id==id) {
            return i;
        }
    }
    return -1;
}
/*unosi novu knjigu u niz, rb je redni broj knjige u nizu*/
void unos_knjige (struct Knjiga knjige [], int rb) {
    if (rb<1024) {
        do {
            printf("Unesi id knjige\n");
            scanf("%d", &knjige[rb].id);
        } while (!(ispravnost_id_knjige(knjige, knjige[rb].id, rb)));
        getchar();
        printf("Unesi naslov knjige\n");
        unesi(knjige[rb].naslov, 50);
        printf("Unesi autore knjige\n");
        unesi(knjige[rb].autori, 100);
        printf("Unesi isbn knjige\n");
        unesi(knjige[rb].isbn, 20);
        knjige[rb].posudjena=-1;
        knjige[rb].datum_posudbe.dan=0;
        knjige[rb].datum_posudbe.mjesec=0;
        knjige[rb].datum_posudbe.godina=0;
    } else
        printf("Napunili smo spremnik!!!\n");
}
/*provjerava ispravnost id knjige*/
int ispravnost_id_knjige (struct Knjiga knjiga [], int id, int vel) {
    int i;
    for (i=0;i<vel;i++) {
        if ((knjiga[i].id==id)||(knjiga[i].id<1)) {
            return 0;
        }
    }
    return 1;
}
/*promjena podataka neke knjige*/
void edituj_knjigu  (struct Knjiga knjiga [], int id, int a) {
    int i;
    printf("Za promjenu naziva pritisnite 1\nZa promjenu autora pritisnite 2\nZa promjenu isbn pritisnite 3\n");
    scanf("%d", &i);
    getchar();
    if (a>-1) {
        do {

            if (i==1) {
                printf("Unesite promjenjeni naziv:\n");
                unesi(knjiga[a].naslov, 50);
            } else if (i==2) {
                printf("Unesite promjenjene autore:\n");
                unesi(knjiga[a].autori, 100);
            } else if (i==3) {
                printf("Unesite promjenjeni isbn:\n");
                unesi(knjiga[a].isbn, 20);
            }
            printf("Kad promjenite sve sto ste zeljeli kod knjige pritisnite 0\n");
            scanf("%d", &i);
        } while (i!=0);
    } else
        printf("Knjiga nije pronadjena sa tim id pa nemoze biti izvrsena izmjena!\n");
}
/*brise knjigu ukoliko nije posudjena*/
int izbrisi_knjigu  (struct Knjiga knjiga [],  int a, int vel1) {
    int i=0, j;
    if (a>-1)
        for (i=0;i<vel1;i++) {
            if ((i==a)&&(knjiga[i].posudjena<0)) {
                for (j=i;j<vel1-1; j++)
                    knjiga[j]=knjiga[j+1];
                vel1--;
            }
        }
    else
        printf("Knjiga nije izbrisana\n!");
    return vel1;
}
/*pronalazi knjigu po idi vraca indeks*/
int nadji_id_knjige (struct Knjiga knjiga [],const int id, const int vel) {
    int i=0;
    for (;i<vel;i++) {
        if (knjiga[i].id==id) {
            return i;
        }
    }
    return -1;
}
/*zaduzuje knjigu*/
void zaduzi_knjigu (struct Knjiga knjiga [], int id, int a) {
    int dan, mjesec, godina;
    if (knjiga[a].posudjena==-1) {
        knjiga[a].posudjena=id;
        do {
            printf("Dan posudbe je\n");
            scanf("%d", &dan);
            knjiga[a].datum_posudbe.dan=dan;
            printf("Mjesec posudbe je\n");
            scanf("%d", &mjesec);
            knjiga[a].datum_posudbe.mjesec=mjesec;
            printf("Godina posudbe je\n");
            scanf("%d", &godina);
            knjiga[a].datum_posudbe.godina=godina;
        } while (!(ispravnost_datuma(knjiga[a].datum_posudbe)));
    } else
        printf("Iznajmljivanje knjige nije uspjelo moguci uzroci toga su:\na)ne postoji knjiga sa tim idom\nb)ne postoji osoba sa tim idom\nc)knjiga je iznajmljena!\n");
}
/*vraca knjigu*/
void vrati_knjigu (struct Knjiga knjiga [], int id, int a) {
    if (knjiga[a].posudjena>0) {
        knjiga[a].posudjena=-1;
        knjiga[a].datum_posudbe.dan=0;
        knjiga[a].datum_posudbe.mjesec=0;
        knjiga[a].datum_posudbe.godina=0;
    } else
        printf("Vracanje knjige nije uspjelo\n");
}
/*pretrazuje osobe po imenu i prezimenu - posto u istoj stavci pise pretraga po imenu i prezimenu*/
void pretraga_po_imenu_i_prezimenu_osobe (const struct Osoba osobe [],const char prezime [], const char ime [], const int vel) {
    int i=0;
    for (;i<vel;i++) {
        if ((strcmp(osobe[i].prezime,prezime)==0) || (strcmp(osobe[i].ime,ime)==0)) {
            printf("%d %5s %20s\n", osobe[i].id, osobe[i].ime, osobe[i].prezime);
        }
    }
}
/*pretrazuje knjige po naslovu, autorima i isbn broju*/
void pretraga_po_nazivu_autorima_isbnu_knjige (const struct Knjiga knjige [],const char naslov [],const char autori [], const char isbn [], const int vel) {
    int i=0;
    for (;i<vel;i++) {
        if ((strcmp(knjige[i].naslov, naslov)==0)||(strcmp(knjige[i].autori, autori)==0)||(strcmp(knjige[i].isbn, isbn)==0)) {
            printf("%d %-5s %-10s %-5s %d ", knjige[i].id, knjige[i].naslov, knjige[i].autori, knjige[i].isbn, knjige[i].posudjena );
            printf("%d. %d. %d. \n", knjige[i].datum_posudbe.dan, knjige[i].datum_posudbe.mjesec, knjige[i].datum_posudbe.godina);
        }
    }
}
/*provjerava da li je osoba posudila knjigu*/
int duzan_knjigu (const struct Knjiga knjige [],const int id, const int vel) {
    int i=0, naso=0;
    for (;i<vel;i++) {
        if (knjige[i].posudjena==id) {
            naso=1;
        }
    }
    return naso;
}
/*pomocna funkcija prestupna godina*/
int prestupna (int godina) {
    if (godina%4==0 && (godina%100!=0 || godina%400==0))
        return 1;
    return 0;
}
/*pomocna ispravnost datuma*/
int ispravnost_datuma (struct Datum datum) {
    if (datum.mjesec<1 || datum.mjesec>12 || datum.godina<1984)
        return 0;
    if (datum.mjesec==1 || datum.mjesec==3 || datum.mjesec==5 || datum.mjesec==7 || datum.mjesec==8 || datum.mjesec==10 || datum.mjesec==12) {
        if (datum.dan<1 || datum.dan>31)
            return 0;
    }
    if (datum.mjesec==4 || datum.mjesec==6 || datum.mjesec==9 || datum.mjesec== 11) {
        if (datum.dan<1 || datum.dan>30)
            return 0;
    }
    if (datum.mjesec==2) {
        if (!prestupna(datum.godina)) {
            if (datum.dan<1 || datum.dan>28)
                return 0;
        }
        if (prestupna(datum.godina)) {
            if (datum.dan<1 || datum.dan>29)
                return 0;
        }
    }
    return 1;
}
/*pretvara protekle mjesece u dane*/
int godine_i_mjeseci_u_dane (struct Datum datum) {
    int i, ukupno=0;
    for (i=1;i<=datum.mjesec;i++) {
        if (i==2) {
            if (prestupna(datum.godina))
                ukupno+=29;
            else
                ukupno+=28;
        }
        if (datum.mjesec==1 || datum.mjesec==3 || datum.mjesec==5 || datum.mjesec==7 || datum.mjesec==8 || datum.mjesec==10 || datum.mjesec==12) {
            ukupno+=31;
        }
        if (datum.mjesec==4 || datum.mjesec==6 || datum.mjesec==9 || datum.mjesec== 11) {
            ukupno+=30;
        }
    }
    return ukupno;
}
/*racuna kolko je proslo dana izmedju referentnog datuma i datuma posudbe*/
int proslo_dana (struct Datum datum) {
    struct Datum referentni;
    referentni.dan=0;
    referentni.mjesec=0;
    referentni.godina=0;
    int i,ukupno=0;
    for (i=0;i<datum.godina;i++) {
        if (prestupna(i)==1)
            ukupno+=366;
        else
            ukupno+=365;
    }
    ukupno+=godine_i_mjeseci_u_dane(datum) - godine_i_mjeseci_u_dane(referentni);
    ukupno+=datum.dan-referentni.dan;
    return ukupno;
}
/*sortira knjige po datumu izdavanja, neizdane knige sz na vrhu*/
void sortiraj (struct Knjiga knjige[], const int velk) {
    int i=0, j=0;
    struct Knjiga temp;
    for (i=0;i<velk-1;i++) {
        for (j=i+1;j<velk;j++) {
            if (proslo_dana(knjige[i].datum_posudbe)>proslo_dana(knjige[j].datum_posudbe)) {
                temp=knjige[i];
                knjige[i]=knjige[j];
                knjige[j]=temp;
            }
        }
    }
}
/*druga verzija funkcije za pretragu osobe po id*/
struct Osoba vrati_osobu_po_id (struct Osoba osobe [],const int id, const int vel) {
    int i=0;
    struct Osoba povratna;
    for (;i<vel;i++) {
        if (osobe[i].id==id) {
            povratna=osobe[i];
            return povratna;
        }
    }
    povratna.id=0;
    return povratna;
}
/*tabelarni ispis knjige*/
void ispis_kniga_nadogradjen  (struct Knjiga knjige [],struct Osoba osobe [], const int velk, const int velo) {
    int i;
    char temp [5];
    struct Osoba posudjena;
    struct Osoba privremeneo [velo];
    struct Knjiga privremenek [velk];
    for (i=0;i<velk;i++) {
        privremenek[i]=knjige[i];
    }
    for (i=0;i<velo;i++) {
        privremeneo[i]=osobe[i];
    }
    for (i=0;i<velk;i++) {
        privremenek[i].naslov[25]='\0';
        printf("%d ", privremenek[i].id);
        sprintf(temp,"%d", privremenek[i].id);
        ispisi_razmak(5-strlen(temp));
        printf("%s ", privremenek[i].naslov);
        ispisi_razmak(25-strlen(privremenek[i].naslov));
        if (knjige[i].posudjena==-1)
            printf("nije posudjena\n");
        else {
            posudjena=vrati_osobu_po_id(osobe, privremenek[i].posudjena, velo);
            privremeneo[i].ime[15]='\0';
            privremeneo[i].prezime[15]='\0';
            printf("%s ",posudjena.ime);
            ispisi_razmak(15-strlen(posudjena.ime));
            printf("%s ", posudjena.prezime);
            ispisi_razmak(15-strlen(posudjena.prezime));
            printf("%d.%d.%d. \n", privremenek[i].datum_posudbe.dan, privremenek[i].datum_posudbe.mjesec, privremenek[i].datum_posudbe.godina);
        }
    }
}
/*funkcija koja ispisuje razmak*/
void ispisi_razmak (int razmaka) {
    int i;
    for (i=1;i<=razmaka;i++)
        printf(" ");
}
/*funkcija koja ispisuje osobu*/
void ispisi_osobu (struct Osoba niz [], int adresa, int vel) {
    int i;
    for (i=0;i<vel;i++) {
        if (i==adresa)
            printf("%d %5s %20s\n", niz[i].id, niz[i].ime, niz[i].prezime);
    }
}
void ispisi_knjigu (struct Knjiga niz [], int adresa, int vel) {
    int i;
    for (i=0;i<vel;i++) {
        if (i==adresa)
            printf("%d %s %s %s %d\n", niz[i].id, niz[i].naslov, niz[i].autori, niz[i].isbn, niz[i].posudjena );
    }
}
/*zamjeni razmak sa _*/
void zamjeni_razmak_ (char *string) {
    char *pom=string;
    int i=0;
    while (*string!='\0') {
        if (*string!=' ') {
            *pom=*string;
        } else {
            *pom='_';
        }
        i++;
        pom++;
        string++;
    }
}
/*zamjeni_"_"s razmakom*/
void zamjeni__razmakom (char *string) {
    char *pom=string;
    int i=0;
    while (*string!='\0') {
        if (*string!='_') {
            *pom=*string;
        } else {
            *pom=' ';
        }
        i++;
        pom++;
        string++;
    }
}
